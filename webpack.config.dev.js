const webpack = require('webpack');
const path = require('path');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',

  entry: ['./src/index.js'],

  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'js/bundle.js',
    chunkFilename: 'js/[name].chunk.js',
    publicPath: '/',
  },

  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    alias: {
      '@': path.join(__dirname, 'src')
    }
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: "babel-loader",
        include: path.resolve(__dirname, "src"),
        options: {
          cacheDirectory: true
        }
      },
      {
        test: /\.(sass|scss)$/,
        use: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(tft|woff|woff2)$/,
        loader: "file-loader",
        options: {
          name: "fonts/[name].[hash:8].[ext]"
        }
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: 'public/index.html',
      inject: true
    }),
    new FriendlyErrorsWebpackPlugin({
      compilationSuccessInfo: {
        messages: ['Server is running here: http://localhost:8080']
      }
    })
  ],

  devtool: 'cheap-module-eval-source-map',

  devServer: {
    compress: true,
    contentBase: path.resolve(__dirname, 'dist'),
    watchContentBase: true,
    historyApiFallback: true,
    host: '0.0.0.0',
    disableHostCheck: true,
    hot: true,
    quiet: true
  }
};