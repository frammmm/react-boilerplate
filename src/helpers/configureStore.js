import {
  applyMiddleware,
  createStore
} from 'redux';
import thunkMiddleware from 'redux-thunk';

import rootReducer from '../reducers/index';

const middlewares = [];

middlewares.push(thunkMiddleware);

if (process.env.NODE_ENV !== 'production') {
  const { logger } = require('redux-logger');

  middlewares.push(logger);
}

const configureStore = (initialState) =>
  createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middlewares)
  );

export default configureStore;