import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";

import configureStore from "@/helpers/configureStore";

import Main from "@/views/Main";

render(
  <Provider store={configureStore()}>
    <Main />
  </Provider>,
  document.getElementById("app")
);
